import React from "react";
import { Redirect } from "react-router-dom";
import { Typography, CircularProgress } from "@material-ui/core";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import "./Loading.css";

function Loading({ userState, authedState }) {
  if (authedState === false) {
    return <Redirect to="/login" />;
  }

  if (userState.id !== null) {
    return <Redirect to="/notes" />;
  }

  return (
    <div className="Loading__Container">
      <div className="Loading__Title">
        <div className="Loading__TitleLogo">
          <Typography variant="h2">Loading</Typography>
          <ExitToAppRoundedIcon fontSize="inherit"></ExitToAppRoundedIcon>
        </div>
      </div>
      <div className="Loading__Spinner">
        <CircularProgress color="primary" />
      </div>
    </div>
  );
}

export default Loading;
