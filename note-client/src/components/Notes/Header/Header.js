import React from "react";
import { IconButton, Typography, AppBar } from "@material-ui/core";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import InvertColorsRoundedIcon from "@material-ui/icons/InvertColorsRounded";
import "./Header.css";

function Header({ onThemeChange, nickname, onLogout }) {
  return (
    <AppBar position="static">
      <div className="Wrapper">
        <div className="Wrapper__Content">
          <div className="Header">
            <div className="Header__User">
              <IconButton onClick={onLogout}>
                <ExitToAppRoundedIcon></ExitToAppRoundedIcon>
              </IconButton>
              <Typography variant="h5">{nickname}</Typography>
            </div>
            <IconButton onClick={onThemeChange}>
              <InvertColorsRoundedIcon />
            </IconButton>
          </div>
        </div>
      </div>
    </AppBar>
  );
}

export default Header;
