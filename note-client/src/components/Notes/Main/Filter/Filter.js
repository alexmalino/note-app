import React from "react";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

function Filter({ checked, onChangeFiler }) {
  const handleChange = (value) => {
    onChangeFiler(value);
  };

  return (
    <FormGroup>
      <FormControlLabel
        control={
          <Switch
            checked={checked}
            onChange={(event) => handleChange(event.target.checked)}
            name="checked"
            color="primary"
          />
        }
        label="Show finished"
      />
    </FormGroup>
  );
}

export default Filter;
