import React, { useState } from "react";
import Overview from "./Overview/Overview";
import Header from "../Header/Header";
import { Paper } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { themes, lightTheme, darkTheme } from "../../../Helpers/Models";
import "./Main.css";

function Main({ userState, onChangeUserState, onLogout }) {
  const [darkMode, setDarkMode] = useState(
    themes.find((x) => x.id === userState.setting.themeId).name === "dark"
  );

  const userHasDarkMode =
    themes.find((x) => x.id === userState.setting.themeId).name === "dark";
  if (darkMode !== userHasDarkMode) {
    setDarkMode(userHasDarkMode);
  }

  const changeTheme = () => {
    const newMode = !darkMode;
    setDarkMode(newMode);
    userState.setting.themeId = newMode ? themes[1].id : themes[0].id;
    onChangeUserState(userState);
  };

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <Paper className="Main__Container">
        <Header
          onThemeChange={changeTheme}
          nickname={userState.nickname}
          onLogout={onLogout}
        />

        <div className="Wrapper">
          <div className="Wrapper__Content">
            <Overview
              userState={userState}
              onChangeUserState={onChangeUserState}
            />
          </div>
        </div>
      </Paper>
    </ThemeProvider>
  );
}

export default Main;
