import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import LensRoundedIcon from "@material-ui/icons/LensRounded";
import EditRoundedIcon from "@material-ui/icons/EditRounded";
import Popover from "@material-ui/core/Popover";
import { DateToFinishDate } from "../../../../Helpers/DateConverter";
import "./Note.css";

function Note({ note, onNoteEdit, onSetDone }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const setDone = (value) => {
    note.done = value;
    onSetDone(note);
  };

  const items = [];

  for (var i = 0; i < note.importance; i++) {
    items.push(<LensRoundedIcon />);
  }

  if (note.doUntil == null) {
    note.doUntil = Date.now();
  }

  const doUntil = new Date(note.doUntil);

  const getDoUntil = () => {
    return DateToFinishDate(doUntil);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div className="Note__Container">
      <div className="Note__Done">
        <Checkbox
          checked={note.done}
          onChange={(event) => setDone(event.target.checked)}
          color="primary"
        />
      </div>

      <div className="Note__DoUntil">{getDoUntil()}</div>

      <div className="Note__Info" onClick={handleClick}>
        <Typography variant="h6" className="No-Text-Wrap">
          {note.title}
        </Typography>
        <Typography variant="subtitle1" className="No-Text-Wrap">
          {note.text}
        </Typography>
      </div>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <Typography className="Note__Popover">{note.text}</Typography>
      </Popover>

      <div className="Note__Importance">
        {Array.from({ length: note.importance }, (v, i) => i).map((i) => {
          return <LensRoundedIcon key={i} color="secondary" fontSize="small" />;
        })}
      </div>

      <div className="Note__Edit">
        <IconButton onClick={onNoteEdit}>
          <EditRoundedIcon color="primary" />
        </IconButton>
      </div>
    </div>
  );
}

export default Note;
