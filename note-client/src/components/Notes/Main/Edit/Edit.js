import React, { useState } from "react";
import { Button, TextField, Typography, Box } from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import { withStyles } from "@material-ui/core/styles";
import LensRoundedIcon from "@material-ui/icons/LensRounded";
import { DateToString } from "../../../../Helpers/DateConverter";
import "./Edit.css";

const StyledRating = withStyles({
  iconFilled: {
    color: "#2196f3",
  },
  iconHover: {
    color: "#2196f3",
  },
})(Rating);

function Edit({ noteState, onSaveNote, onDeleteNote, onCancel }) {
  const [note, setNote] = useState(noteState);

  const saveNote = () => {
    onSaveNote(note);
  };

  const deleteNote = () => {
    if (note.id != null) {
      onDeleteNote(note.id);
    }
  };

  const saveBtnEnable = note.title.length > 0;
  const today = new Date(noteState.doUntil);
  const doUntil = DateToString(today);

  return (
    <div className="Edit__Container">
      <TextField
        label="Title"
        style={{ margin: 8 }}
        placeholder="Enter title"
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        variant="outlined"
        defaultValue={noteState.title}
        onChange={(event) => setNote({ ...note, title: event.target.value })}
      />

      <TextField
        label="Description"
        style={{ margin: 8 }}
        placeholder="Enter description"
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true,
        }}
        multiline
        rowsMax={7}
        variant="outlined"
        defaultValue={noteState.text}
        onChange={(event) => setNote({ ...note, text: event.target.value })}
      />

      <Box ml={1} mt={2}>
        <Typography>Importance</Typography>
        <StyledRating
          name="importance"
          defaultValue={noteState.importance}
          precision={1}
          size="large"
          emptyIcon={<LensRoundedIcon fontSize="inherit" />}
          icon={<LensRoundedIcon fontSize="inherit" />}
          onChange={(event) =>
            setNote({ ...note, importance: +event.target.value })
          }
        />
      </Box>

      <Box ml={1} mt={2}>
        <form noValidate>
          <TextField
            id="date"
            label="Finish Date"
            type="date"
            defaultValue={doUntil}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(event) =>
              setNote({ ...note, doUntil: event.target.value })
            }
          />
        </form>
      </Box>

      <div className="Edit__Buttons">
        {note.id != null && (
          <Button onClick={deleteNote} color="inherit" variant="outlined">
            Delete
          </Button>
        )}
        <Button variant="outlined" onClick={onCancel}>
          Cancel
        </Button>
        <Button
          onClick={saveNote}
          disabled={!saveBtnEnable}
          color="primary"
          variant="contained"
        >
          Save
        </Button>
      </div>
    </div>
  );
}

export default Edit;
