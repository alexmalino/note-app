import React, { useState } from "react";
import Note from "../Note/Note";
import Edit from "../Edit/Edit";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Sort from "../Sort/Sort";
import Filter from "../Filter/Filter";
import {
  SortByFinishDate,
  SortByCreateDate,
  SortByImportance,
} from "../../../../Helpers/NoteHelper";
import {
  GetNotes,
  AddNote,
  UpdateNote,
  DeleteNote,
} from "../../../../Helpers/Api";
import { sortTypes } from "../../../../Helpers/Models";
import { DateToString } from "../../../../Helpers/DateConverter";

import "./Overview.css";

function Overview({ userState, onChangeUserState }) {
  const [editState, setEditState] = useState(false);
  const [notesState, setNotesState] = useState(null);
  const [selectedNoteState, setSelectedNoteState] = useState([]);
  const today = new Date(Date.now());

  const defaultNote = {
    id: null,
    title: "",
    text: "",
    done: false,
    importance: 1,
    createDate: null,
    doUntil: DateToString(today),
  };

  if (userState.id != null && notesState === null) {
    GetNotes(userState.id).then((notes) => setNotesState(notes));
  }

  const addNote = () => {
    setSelectedNoteState(defaultNote);
    setEditState(true);
  };

  const editNote = (noteId) => {
    const note = notesState.find((n) => n.id === noteId);
    setSelectedNoteState(note);
    setEditState(true);
  };

  const saveNote = (note) => {
    if (note.id == null) {
      AddNote(userState.id, note).then((result) => {
        if (result != null) {
          notesState.push(result);
          setNotesState([...notesState]);
        } else {
          alert(
            result != null && result.message != null
              ? result.message
              : "Something went wrong!"
          );
        }
      });
    } else {
      UpdateNote(userState.id, note.id, note).then((result) => {
        if (result != null) {
          const index = notesState.findIndex((n) => n.id === result.id);
          notesState[index] = result;
          setNotesState([...notesState]);
        } else {
          alert(
            result != null && result.message != null
              ? result.message
              : "Something went wrong!"
          );
        }
      });
    }
    setEditState(false);
  };

  const deleteNote = (noteId) => {
    if (noteId != null) {
      DeleteNote(userState.id, noteId).then((success) => {
        if (success) {
          setNotesState([...notesState.filter((n) => n.id !== noteId)]);
        } else {
          alert("Something went wrong!");
        }
      });
    }
    setEditState(false);
  };

  const changeSortType = (sortTypeId) => {
    userState.setting.sortTypeId = sortTypeId;
    onChangeUserState(userState);
  };

  const changeFilter = (value) => {
    userState.setting.showFinished = value;
    onChangeUserState(userState);
  };

  const sortNotes = (a, b) => {
    if (userState.setting.sortTypeId === sortTypes[0].id) {
      return SortByFinishDate(a, b);
    }

    if (userState.setting.sortTypeId === sortTypes[1].id) {
      return SortByCreateDate(a, b);
    }

    if (userState.setting.sortTypeId === sortTypes[2].id) {
      return SortByImportance(a, b);
    }
  };

  if (editState) {
    return (
      <Edit
        noteState={{ ...selectedNoteState }}
        onSaveNote={(note) => saveNote(note)}
        onDeleteNote={(noteId) => deleteNote(noteId)}
        onCancel={() => setEditState(false)}
      ></Edit>
    );
  }

  return (
    <>
      <div className="Overview__Toolbar">
        <Sort
          sortTypeId={userState.setting.sortTypeId}
          onSortTypeChange={changeSortType}
        />
        <Filter
          checked={userState.setting.showFinished}
          onChangeFiler={changeFilter}
        />
      </div>

      <div className="Overview__Wrapper">
        {notesState &&
          notesState
            .filter(
              (note) =>
                (userState.setting.showFinished && note.done) ||
                !userState.setting.showFinished
            )
            .sort((a, b) => sortNotes(a, b))
            .map((note) => {
              return (
                <Note
                  key={note.id}
                  note={note}
                  onNoteEdit={() => editNote(note.id)}
                  onSetDone={(note) => saveNote(note)}
                />
              );
            })}
        <div className="Overview__AddButton">
          <Fab color="primary" size="medium" onClick={addNote}>
            <AddIcon />
          </Fab>
        </div>
      </div>
    </>
  );
}

export default Overview;
