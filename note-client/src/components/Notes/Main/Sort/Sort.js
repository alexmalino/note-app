import React from "react";
import { Select, FormControl, MenuItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { sortTypes } from "../../../../Helpers/Models";

export default function Sort({ sortTypeId, onSortTypeChange }) {
  const sortType = sortTypes.find((t) => t.id === sortTypeId);

  const handleChange = (value) => {
    const newSortType = sortTypes.find((x) => x.id === value);
    onSortTypeChange(newSortType.id);
  };

  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(2),
      minWidth: 200,
    },
  }));

  const classes = useStyles();

  return (
    <FormControl className={classes.formControl}>
      <Select
        labelId="sort-label"
        id="sort-select"
        name={sortType.name}
        value={sortType.id}
        onChange={(event) => handleChange(event.target.value)}
      >
        {sortTypes.map((type) => {
          return (
            <MenuItem key={type.id} value={type.id}>
              {type.name}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
}
