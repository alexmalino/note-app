import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import { FormControl, InputLabel, Input, Button } from "@material-ui/core";
import { LoginUser } from "../../Helpers/Api";
import { Redirect } from "react-router-dom";
import "./Login.css";

function Login({ onChangeUserState, onLogin }) {
  const [routerState, setRouterState] = useState("login");

  const [userState, setUserState] = useState({
    emailAddress: "",
    password: "",
  });

  const loginUser = () => {
    const user = {
      emailAddress: userState.emailAddress,
      password: userState.password,
    };

    LoginUser(user).then((result) => {
      if (
        result != null &&
        result.token != null &&
        result.userToReturn != null
      ) {
        localStorage.setItem("token", result.token);
        localStorage.setItem("userId", result.userToReturn.id);
        onChangeUserState(result.userToReturn);
        onLogin();
        setRouterState("note");
      } else {
        const message =
          result.message != null ? result.message : "Something went wrong";
        alert(message);
      }
    });
  };

  if (routerState !== "login") {
    return <Redirect to={"/" + routerState} />;
  }

  return (
    <div className="Login__Container">
      <div className="Login__Title">
        <div className="Login__TitleLogo">
          <Typography variant="h2">Login</Typography>
          <ExitToAppRoundedIcon fontSize="inherit"></ExitToAppRoundedIcon>
        </div>
      </div>
      <div className="Login__Form">
        <FormControl>
          <InputLabel>Email</InputLabel>
          <Input
            onChange={(event) =>
              setUserState({
                ...userState,
                emailAddress: event.target.value,
              })
            }
          />
        </FormControl>
        <FormControl>
          <InputLabel>Password</InputLabel>
          <Input
            type="password"
            onChange={(event) =>
              setUserState({
                ...userState,
                password: event.target.value,
              })
            }
          />
        </FormControl>
        <div className="Login__Buttons">
          <Button variant="outlined" onClick={() => setRouterState("register")}>
            Register
          </Button>
          <Button
            disabled={
              userState.emailAddress < 5 || userState.password.length < 4
            }
            color="primary"
            variant="contained"
            onClick={() => loginUser()}
          >
            Login
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Login;
