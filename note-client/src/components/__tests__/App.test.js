import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render } from "@testing-library/react";
import Overview from "../Notes/Main/Overview/Overview";
import Header from "../Notes/Header/Header";
import Note from "../Notes/Main/Note/Note";

/*** Setup ***/
const userState = {
  id: 1,
  nickname: "Mustermann",
  emailAddress: "Max.Mustermann@gmail.com",
  setting: {
    sortTypeId: 1,
    themeId: 1,
    showFinished: false,
  },
  created: "2020-09-05T20:50:44.4703859",
};

const note = {
  id: 1,
  title: "Dummy Note",
  text: "Dummy Description",
  done: false,
  importance: 1,
  createDate: "2020-09-15",
};

/* Header Tests */
test("renders logged in user", () => {
  const { getByText } = render(<Header nickname={userState.nickname} />);
  const element = getByText(/Mustermann/);
  expect(element).toBeInTheDocument();
});

/* Overview Tests*/
test('renders sort type "By Finish Date"', () => {
  userState.setting.sortTypeId = 1;
  const { getByText } = render(<Overview userState={userState} />);
  const element = getByText(/By Finish Date/);
  expect(element).toBeInTheDocument();
});

test('renders sort type "By Create Date"', () => {
  userState.setting.sortTypeId = 2;
  const { getByText } = render(<Overview userState={userState} />);
  const element = getByText(/By Create Date/);
  expect(element).toBeInTheDocument();
});

test('renders sort type "By Importance"', () => {
  userState.setting.sortTypeId = 3;
  const { getByText } = render(<Overview userState={userState} />);
  const element = getByText(/By Importance/);
  expect(element).toBeInTheDocument();
});

test("renders single note", () => {
  const { getByText } = render(<Note note={note} />);

  const title = getByText(/Dummy Note/);
  const text = getByText(/Dummy Description/);

  expect(title).toBeTruthy();
  expect(text).toBeTruthy();
});
