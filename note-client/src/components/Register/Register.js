import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import BorderColorRoundedIcon from "@material-ui/icons/BorderColorRounded";
import { FormControl, InputLabel, Input, Button } from "@material-ui/core";
import { AddUser } from "../../Helpers/Api";
import { Redirect } from "react-router-dom";
import "./Register.css";

function Register() {
  const [routerState, setRouterState] = useState("register");

  const [userState, setUserState] = useState({
    nickname: "",
    emailAddress: "",
    password: "",
    confirmedPassword: "",
  });

  const [validationState, setValidationState] = useState({
    nicknameValid: false,
    emailAddressValid: false,
    passwordValid: false,
    confirmedPasswordValid: false,
  });

  const changeNickname = (value) => {
    setUserState({ ...userState, nickname: value });

    setValidationState({
      ...validationState,
      nicknameValid: value.length > 0,
    });
  };

  const changeEmail = (value) => {
    setUserState({ ...userState, emailAddress: value });

    setValidationState({
      ...validationState,
      emailAddressValid:
        value.includes("@") && value.includes(".") && value.length >= 5,
    });
  };

  const changePassword = (value) => {
    setUserState({
      ...userState,
      password: value,
    });

    setValidationState({
      ...validationState,
      passwordValid: value.length >= 4 && value.length <= 20,
    });
  };

  const changeConfirmedPassword = (value) => {
    setUserState({
      ...userState,
      confirmedPassword: value,
    });

    setValidationState({
      ...validationState,
      confirmedPasswordValid: value === userState.password,
    });
  };

  const registerUser = () => {
    const user = {
      nickname: userState.nickname,
      emailAddress: userState.emailAddress,
      password: userState.password,
    };

    AddUser(user).then((response) => {
      if (response.message != null) {
        alert(response.message);
      } else {
        setRouterState("login");
      }
    });
  };

  if (routerState === "login") {
    return <Redirect to="/login" />;
  }

  return (
    <div className="Register__Container">
      <div className="Register__Title">
        <div className="Register__TitleLogo">
          <Typography variant="h2">Register</Typography>
          <BorderColorRoundedIcon fontSize="inherit"></BorderColorRoundedIcon>
        </div>
      </div>
      <div className="Register__Form">
        <FormControl>
          <InputLabel>Nickname</InputLabel>
          <Input
            type="text"
            error={!validationState.nicknameValid}
            onChange={(event) => changeNickname(event.target.value)}
          />
        </FormControl>
        <FormControl>
          <InputLabel>Email</InputLabel>
          <Input
            type="email"
            error={!validationState.emailAddressValid}
            onChange={(event) => changeEmail(event.target.value)}
          />
        </FormControl>
        <FormControl>
          <InputLabel>Password</InputLabel>
          <Input
            type="password"
            error={!validationState.passwordValid}
            onChange={(event) => changePassword(event.target.value)}
          />
        </FormControl>
        <FormControl>
          <InputLabel>Confirm Password</InputLabel>
          <Input
            type="password"
            error={!validationState.confirmedPasswordValid}
            onChange={(event) => changeConfirmedPassword(event.target.value)}
          />
        </FormControl>
        <div className="Register__Buttons">
          <Button
            color="primary"
            variant="outlined"
            onClick={() => setRouterState("login")}
          >
            Login
          </Button>
          <Button
            disabled={
              !(
                validationState.nicknameValid &&
                validationState.emailAddressValid &&
                validationState.passwordValid &&
                validationState.confirmedPasswordValid
              )
            }
            variant="contained"
            onClick={() => registerUser()}
          >
            Register
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Register;
