import { blue, grey } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core/styles";

export const sortTypes = [
  { id: 1, name: "By Finish Date" },
  { id: 2, name: "By Create Date" },
  { id: 3, name: "By Importance" },
];

export const themes = [
  { id: 1, name: "light" },
  { id: 2, name: "dark" },
];

export const lightTheme = createMuiTheme({
  palette: {
    primary: blue,
    type: "light",
    secondary: {
      main: "#2c387e",
    },
  },
});

export const darkTheme = createMuiTheme({
  palette: {
    primary: grey,
    type: "dark",
    secondary: {
      main: "#2196f3",
    },
  },
});
