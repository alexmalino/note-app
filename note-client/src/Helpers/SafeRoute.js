import React from "react";
import { Route, Redirect } from "react-router-dom";

function SafeRoute({ component: Component, authedState, ...rest }) {
  if (!authedState) {
    return <Redirect to="/login" />;
  }

  return <Route {...rest} render={() => Component} />;
}

export default SafeRoute;
