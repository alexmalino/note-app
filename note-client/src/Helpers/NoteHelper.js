export function SortByFinishDate(note1, note2) {
  if (note1.doUntil < note2.doUntil) {
    return -1;
  }

  if (note1.doUntil > note2.doUntil) {
    return 1;
  }

  return 0;
}

export function SortByCreateDate(note1, note2) {
  if (note1.createDate < note2.createDate) {
    return -1;
  }

  if (note1.createDate > note2.createDate) {
    return 1;
  }

  return 0;
}

export function SortByImportance(note1, note2) {
  if (note1.importance < note2.importance) {
    return 1;
  }

  if (note1.importance > note2.importance) {
    return -1;
  }

  return 0;
}
