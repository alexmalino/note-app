export const BaseUrl = "/api";

let token = "";

export function SetToken(newToken) {
  token = newToken;
}

export async function GetUser(userId) {
  try {
    const response = await fetch(BaseUrl + "/users/" + userId, {
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();

    if (response.ok) {
      return data;
    }

    return null;
  } catch (error) {
    console.error("GetUser() failed: ", error.message);
  }
}

export async function AddUser(user = {}) {
  try {
    const response = await fetch(BaseUrl + "/users", {
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(user),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    console.error("AddUser() failed: ", error);
  }
}

export async function LoginUser(user = {}) {
  try {
    const response = await fetch(BaseUrl + "/users/login", {
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(user),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("LoginUser() failed: ", error);
  }
}

export async function UpdateUser(userId, user = {}) {
  try {
    const response = await fetch(BaseUrl + "/users/" + userId, {
      method: "PUT",
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(user),
    });

    if (response.ok) {
      return response.json();
    }

    return null;
  } catch (error) {
    console.error("UpdateUser() failed: ", error);
  }
}

export async function DeleteUser(userId) {
  try {
    const response = await fetch(BaseUrl + "/users/" + userId, {
      method: "DELETE",
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${token}`,
      },
    });

    if (response.ok) {
      return response.json();
    }

    return null;
  } catch (error) {
    console.error("DeleteUser() failed: ", error);
  }
}

export async function GetNotes(userId) {
  try {
    const response = await fetch(BaseUrl + "/users/" + userId + "/notes", {
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${token}`,
      },
    });

    const data = await response.json();

    if (response.ok) {
      return data;
    }

    return null;
  } catch (error) {
    console.error("GetUsers() failed: ", error);
  }
}

export async function GetNote(userId, noteId) {
  try {
    const response = await fetch(
      BaseUrl + "/users/" + userId + "/notes/" + noteId,
      {
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`,
        },
      }
    );

    const data = await response.json();

    if (response.ok) {
      return data;
    }

    return null;
  } catch (error) {
    console.error("GetUser() failed: ", error);
  }
}

export async function AddNote(userId, note = {}) {
  try {
    const response = await fetch(BaseUrl + "/users/" + userId + "/notes", {
      method: "POST",
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(note),
    });

    if (response.ok) {
      return response.json();
    }

    return null;
  } catch (error) {
    console.error("AddNote() failed: ", error);
  }
}

export async function UpdateNote(userId, noteId, note = {}) {
  try {
    const response = await fetch(
      BaseUrl + "/users/" + userId + "/notes/" + noteId,
      {
        method: "PUT",
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(note),
      }
    );

    if (response.ok) {
      return response.json();
    }

    return null;
  } catch (error) {
    console.error("UpdateNote() failed: ", error);
  }
}

export async function DeleteNote(userId, noteId) {
  try {
    const response = await fetch(
      BaseUrl + "/users/" + userId + "/notes/" + noteId,
      {
        method: "DELETE",
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${token}`,
        },
      }
    );

    if (response.ok) {
      return true;
    }
    return false;
  } catch (error) {
    console.error("DeleteNote() failed: ", error);
  }
}
