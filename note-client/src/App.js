import React, { useState } from "react";
import SafeRoute from "./Helpers/SafeRoute";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";
import Loading from "./components/Loading/Loading";
import Main from "./components/Notes/Main/Main";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { GetUser, UpdateUser, SetToken } from "./Helpers/Api";
import "./App.css";
import "fontsource-roboto";

function App() {
  const token = localStorage.getItem("token");
  const userId = localStorage.getItem("userId");
  SetToken(token);

  const defaultUser = {
    id: null,
    nickname: "",
    emailAddress: "",
    setting: {
      sortTypeId: 1,
      themeId: 1,
      showFinished: false,
    },
    created: null
  }

  const [authedState, setAuthedState] = useState(token != null && token !== "");
  const [userState, setUserState] = useState(defaultUser);

  if (authedState && userState.id == null && userId != null) {
    GetUser(userId).then((user) => {
      if (user != null) {   
        setUserState(user);
      } else {
        onLogout();
      }
    });
  }

  const onChangeUserState = (user) => {
    if (userId != null) {
      UpdateUser(userId, user).then((user) => {
        if (user != null) {
          setUserState(user);
        }
      });
    }
  };

  const onLogout = () => {
    localStorage.removeItem("userId");
    localStorage.removeItem("token");
    setAuthedState(false);
    setUserState(defaultUser);
  };

  const onLogin = () => {
    setAuthedState(true);
  };

  return (
    <BrowserRouter>
      <Switch>
        <Route path="/loading">
          <Loading userState={userState} authedState={authedState} />
        </Route>
        <Route path="/login">
          <Login onChangeUserState={onChangeUserState} onLogin={onLogin} />
        </Route>
        <Route path="/register" component={Register} />
        <SafeRoute
          authedState={authedState}
          path="/notes"
          component={Main({ userState, onChangeUserState, onLogout })}
        />
        <Route path="/">
          <Redirect to="/loading" />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
