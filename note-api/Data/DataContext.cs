﻿using Microsoft.EntityFrameworkCore;
using note_api.Models;
using System.Security.Cryptography.X509Certificates;

namespace note_api.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options){}

        public DbSet<User> Users { get; set; }

        public DbSet<Note> Notes { get; set; }

        public DbSet<Setting> Settings { get; set; }

        public DbSet<SortType> SortTypes { get; set; }

        public DbSet<Theme> Themes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {   
            builder.Entity<Note>()
            .HasOne(u => u.User)
            .WithMany(n => n.Notes)
            .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Setting>()
            .HasOne(u => u.User)
            .WithOne(s => s.Setting)
            .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
