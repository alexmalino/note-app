﻿using note_api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace note_api.Data
{
    public interface INotesRepository
    {
        Task<List<Note>> GetNotes(int userId);

        Task<Note> GetNote(int userId, int noteId);

        Task<Note> AddNote(Note note);

        Task<Note> UpdateNote(int noteId, Note note);

        Task<bool> DeleteNote(int noteId);       
    }
}
