﻿using Microsoft.EntityFrameworkCore;
using note_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace note_api.Data
{
    public class NotesRepository : INotesRepository
    {
        private readonly DataContext context;

        public NotesRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<List<Note>> GetNotes(int userId)
        {
            using (this.context)
            {
                var notes = await this.context
                    .Users
                    .Include(n => n.Notes)
                    .FirstOrDefaultAsync(u => u.Id == userId);

                if (notes == null)
                {
                    return new List<Note>();
                }

                return notes.Notes.ToList();
            }
        }

        public async Task<Note> GetNote(int userId, int noteId)
        {
            using (this.context)
            {                     
                return await this.context.Notes.FirstOrDefaultAsync(n => n.Id == noteId && n.UserId == userId);
            }
        }

        public async Task<Note> AddNote(Note note)
        {
            using (this.context)
            {
                try
                {
                    await this.context.Notes.AddAsync(note);
                    await this.context.SaveChangesAsync();
                }
                catch (Exception)
                {

                    return null;
                }

                return note;
            }
        }

        public async Task<Note> UpdateNote(int noteId, Note note)
        {
            using (this.context)
            {
                var noteToUpdate = await this.context.Notes.FirstOrDefaultAsync(n => n.Id == noteId);
                if (noteToUpdate != null)
                {
                    try
                    {
                        noteToUpdate.Title = note.Title;
                        noteToUpdate.Text = note.Text;
                        noteToUpdate.Importance = note.Importance; 
                        noteToUpdate.Done = note.Done;
                        noteToUpdate.DoUntil = note.DoUntil;
                        await this.context.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }

                return noteToUpdate;
            }
        }

        public async Task<bool> DeleteNote(int noteId)
        {
            bool success = false;
            using (this.context)
            {
                try
                {
                    var noteToDelete = await this.context.Notes.FirstOrDefaultAsync(n => n.Id == noteId);
                    this.context.Remove(noteToDelete);
                    await this.context.SaveChangesAsync();
                    success = true;
                    return success;
                }
                catch (Exception)
                {
                    success = false;
                    return success;
                }              
            }
        }       
    }
}
