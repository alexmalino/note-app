﻿using note_api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace note_api.Data
{
    public interface IUserRepository
    {
        Task<List<User>> GetUsers();

        Task<User> GetUser(int userId);

        Task<User> Register(User user, string password);

        Task<User> Login(string emailAddress, string password);

        string CreateToken(string userId, string email);

        Task<User> UpdateUser(int userId, User setting);

        Task<bool> DeleteUser(int userId);

        Task<bool> UserExists(string emailAddress);
    }
}
