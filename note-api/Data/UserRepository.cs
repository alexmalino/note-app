﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using note_api.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace note_api.Data
{ 
    public class UserRepository: IUserRepository
    {
        private readonly DataContext context;
        private readonly IConfiguration config;

        public UserRepository(DataContext context, IConfiguration config)
        {
            this.context = context;
            this.config = config;
        }

        public async Task<List<User>> GetUsers()
        {
            using (this.context)
            {                
                return await this.context
                    .Users
                    .Include(u => u.Setting)
                    .Select(u => u).ToListAsync();
            }
        }

        public async Task<User> GetUser(int userId)
        {
            using (this.context)
            {
                return await this.context
                    .Users
                    .Include(u => u.Setting)
                    .FirstOrDefaultAsync(n => n.Id == userId);
            }
        }

        public async Task<User> Register(User user, string password)
        {
            byte[] passwordHash, passwordSalt;
            this.CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;          

            using (this.context)
            {
                try
                {
                    await this.context.Users.AddAsync(user);
                    await this.context.SaveChangesAsync();
                }
                catch (Exception)
                {
                    return null;
                }

                return user;
            }
        }

        public async Task<User> Login(string emailAddress, string password)
        {
            var user = await this.context
                .Users
                .Include(u => u.Setting)
                .FirstOrDefaultAsync(x => x.EmailAddress == emailAddress);

            if (user == null)
            {
                return null;
            }

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }

            return user;
        }

        public async Task<User> UpdateUser(int userId, User user)
        {
            using (this.context)
            {
                var userToUpdate = await this.context
                    .Users
                    .Include(u => u.Setting)
                    .FirstOrDefaultAsync(n => n.Id == userId);

                if (userToUpdate != null)
                {
                    try
                    {
                        userToUpdate.Nickname = user.Nickname;                       
                        userToUpdate.Setting = user.Setting;                       
                        await this.context.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }

                return userToUpdate;
            }
        }

        public async Task<bool> DeleteUser(int userId)
        {
            bool success = false;
            using (this.context)
            {
                try
                {
                    var userToDelete = await this.context.Users.FirstOrDefaultAsync(n => n.Id == userId);
                    this.context.Remove(userToDelete);
                    await this.context.SaveChangesAsync();
                    success = true;
                    return success;
                }
                catch (Exception)
                {
                    success = false;
                    return success;
                }
            }
        }

        public async Task<bool> UserExists(string emailAddress)
        {
            if (await this.context.Users.AnyAsync(u => u.EmailAddress == emailAddress))
            {
                return true;
            }

            return false;
        }        

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    };
                }
            }

            return true;
        }

        public string CreateToken(string userId, string emailAddress)
        {
            // Create claim from userid and email address
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userId),
                new Claim(ClaimTypes.Name, emailAddress)
            };

            // Create token
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
