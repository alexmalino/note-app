﻿using System.ComponentModel.DataAnnotations;

namespace note_api.Models
{
    public class SortType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
