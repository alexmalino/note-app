﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace note_api.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string Nickname { get; set; }

        public string EmailAddress { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

        public Setting Setting { get; set; }

        public virtual ICollection<Note> Notes { get; set; }

        public DateTime Created { get; set; }
    }
}
