﻿using System;
using System.ComponentModel.DataAnnotations;

namespace note_api.Models
{
    public class Note
    {
        [Key]
        public int Id { get; set; }
     
        public int UserId { get; set; }

        public User User { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }
        
        public bool Done { get; set; }

        public int Importance { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime DoUntil { get; set; }
    }
}
