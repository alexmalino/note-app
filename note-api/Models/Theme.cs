﻿using System.ComponentModel.DataAnnotations;

namespace note_api.Models
{
    public class Theme
    {
        [Key]
        public int Id{ get; set; }

        public string Name { get; set; }
    }
}
