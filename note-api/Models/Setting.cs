﻿using System.ComponentModel.DataAnnotations;

namespace note_api.Models
{
    public class Setting
    {
        [Key]
        public int SettingId { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int SortTypeId { get; set; }

        public SortType SortType { get; set; }

        public int ThemeId { get; set; }

        public Theme Theme { get; set; }

        public bool ShowFinished { get; set; }
    }
}
