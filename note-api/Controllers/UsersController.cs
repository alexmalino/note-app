﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using note_api.Data;
using note_api.Dtos;
using note_api.Models;

namespace note_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository userRepo;
        private ResponseDto response;

        public UsersController(IUserRepository userRepo)
        {
            this.userRepo = userRepo;
            this.response = new ResponseDto();      
        }

        // GET: api/users
        [HttpGet]
        public async Task<IActionResult> Get()
        {         
            try
            {
                var users = await this.userRepo.GetUsers();
                var usersToReturn = users.Select(u => this.GetUserToReturn(u));
                return Ok(usersToReturn);
            }
            catch (Exception)
            {                   
                this.response.Message = "Get users failed.";
                return BadRequest(this.response);
            }

        }

        // GET api/users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var logedIndUser = User.FindFirst(ClaimTypes.NameIdentifier);

            if (logedIndUser == null || id != int.Parse(logedIndUser.Value))
            {
                return Unauthorized();
            }

            var user = await this.userRepo.GetUser(id);

            if (user == null)
            {               
                this.response.Message = "User does not exists.";
                return BadRequest(this.response);
            }

            var userToReturn = this.GetUserToReturn(user);

            return Ok(userToReturn);
        }

        // POST api/users
        [HttpPost]
        public async Task<IActionResult> Post(UserForRegisterDto userForRegisterDto)
        {
            userForRegisterDto.EmailAddress = userForRegisterDto.EmailAddress.ToLower();

            if (await this.userRepo.UserExists(userForRegisterDto.EmailAddress))
            {
                this.response.Message = "E-mail address already exists";
                return BadRequest(this.response);           
            }

            var userToCreate = this.GetUserToCreate(userForRegisterDto);

            var createdUser = await this.userRepo.Register(userToCreate, userForRegisterDto.Password);

            var userToReturn = this.GetUserToReturn(createdUser);

            return Ok(userToReturn);
        }

        // POST api/users/login
        [HttpPost("login", Name = "Login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
        {
            var userFromRepo = await this.userRepo.Login(userForLoginDto.EmailAddress.ToLower(), userForLoginDto.Password);

            if (userFromRepo == null)
            {
                this.response.Message = "User not registered";
                return BadRequest(this.response);
            }         

            var token = this.userRepo.CreateToken(userFromRepo.Id.ToString(), userFromRepo.EmailAddress);
            var userToReturn = this.GetUserToReturn(userFromRepo);

            return Ok(new { token, userToReturn });
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserForClientDto userToUpdate)
        {
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var setting = new Setting()
            {
                ThemeId = userToUpdate.Setting.ThemeId,
                SortTypeId = userToUpdate.Setting.SortTypeId,
                ShowFinished = userToUpdate.Setting.ShowFinished
            };

            var user = new User()
            {
                Nickname = userToUpdate.Nickname,
                Setting = setting
            };

            var updatedUser = await this.userRepo.UpdateUser(id, user);

            if (updatedUser == null)
            {                
                this.response.Message = "Failed to update user.";
                return BadRequest(this.response);
            }

            var userToReturn = this.GetUserToReturn(updatedUser);

            return Ok(userToReturn);
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var success = await this.userRepo.DeleteUser(id);

            if (!success)
            {
                this.response.Message = "Failed to delete user.";
                return BadRequest(this.response);
            }

            this.response.Message = "User deleted";
            return Ok(this.response);
        }

        private UserForClientDto GetUserToReturn(User user)
        {
            var settings = user.Setting;
            return new UserForClientDto()
            {
                Id = user.Id,
                Nickname = user.Nickname,
                EmailAddress = user.EmailAddress,
                Created = user.Created,
                Setting = new SettingDto()
                {
                    SortTypeId = settings.SortTypeId,
                    ThemeId = settings.ThemeId,
                    ShowFinished = settings.ShowFinished
                }
            };
        }

        private User GetUserToCreate(UserForRegisterDto user)
        {
            return new User()
            {
                Nickname = user.Nickname,
                EmailAddress = user.EmailAddress,
                Created = DateTime.Now,
                Setting = new Setting()
                {
                    ThemeId = 1,
                    SortTypeId = 1,
                    ShowFinished = false
                }
            };
        }       
    }
}
