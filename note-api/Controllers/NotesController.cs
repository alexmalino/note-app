﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using note_api.Data;
using note_api.Dtos;
using note_api.Models;

namespace note_api.Controllers
{
    [Route("api/users/{userId:int}/notes")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INotesRepository notesRepo;
        private ResponseDto response;

        public NotesController(INotesRepository notesRepo)
        {
            this.notesRepo = notesRepo;
            this.response = new ResponseDto();
        }

        // GET: api/users/{userId:int}/notes
        [HttpGet]
        public async Task<IActionResult> Get(int userId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var notes = await this.notesRepo.GetNotes(userId);

            var notesToReturn = notes.Select(n => this.CreateNoteToReturn(n));

            return Ok(notesToReturn);
        }

        // GET api/users/{userId:int}/notes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var note = await this.notesRepo.GetNote(userId, id);

            if (note == null)
            {
                this.response.Message = "Note does not exist.";
                return BadRequest(this.response);
            }

            var noteToReturn = this.CreateNoteToReturn(note);

            return Ok(noteToReturn);
        }

        // POST api/users/{userId:int}/notes
        [HttpPost]
        public async Task<IActionResult> Post(int userId, [FromBody] NoteToCreateDto note)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var noteToCreate = this.CreateNote(userId, note);

            var createdNote = await this.notesRepo.AddNote(noteToCreate);

            if (createdNote == null)
            {          
                this.response.Message = "Failed to add note.";
                return BadRequest(this.response);
            }

            var noteToReturn = this.CreateNoteToReturn(createdNote);

            return Ok(noteToReturn);
        }

        // PUT api/users/{userId:int}/notes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int userId, int id, [FromBody] Note note)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var updatedNote = await this.notesRepo.UpdateNote(id, note);

            if (updatedNote == null)
            {              
                this.response.Message = "Failed to update note.";
                return BadRequest(this.response);
            }

            return Ok(updatedNote);
        }

        // DELETE api/users/{userId:int}/notes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return Unauthorized();
            }

            var success = await this.notesRepo.DeleteNote(id);

            if (!success)
            {              
                this.response.Message = "Failed to delete note.";
                return BadRequest(this.response);
            }

            this.response.Message = "Note deleted";
            return Ok(this.response);
        }

        private Note CreateNote(int userId, NoteToCreateDto note)
        {
            return new Note()
            {
                UserId = userId,
                Title = note.Title,
                Text = note.Text,
                Done = false,
                Importance = note.Importance,
                CreateDate = DateTime.Now,
                DoUntil = note.DoUntil
            };
        }

        private NoteToReturnDto CreateNoteToReturn(Note note)
        {
            return new NoteToReturnDto()
            {
                Id = note.Id,
                UserId = note.UserId,
                Title = note.Title,
                Text = note.Text,
                Done = note.Done,
                Importance = note.Importance,
                CreateDate = note.CreateDate,
                DoUntil = note.DoUntil
            };
        }
    }
}
