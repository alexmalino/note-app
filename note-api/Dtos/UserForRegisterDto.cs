﻿using System.ComponentModel.DataAnnotations;

namespace note_api.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "You must specify password between 4 and 20 characters")]
        public string Password { get; set; }

        [Required]
        public string Nickname { get; set; }
    }
}
