﻿using System;

namespace note_api.Dtos
{
    public class NoteToReturnDto
    {
        public int Id { get; set; }

        public int UserId { get; set; }
     
        public string Title { get; set; }

        public string Text { get; set; }

        public bool Done { get; set; }

        public int Importance { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime DoUntil { get; set; }
    }
}
