﻿using System;
using System.ComponentModel.DataAnnotations;

namespace note_api.Dtos
{
    public class NoteToCreateDto
    {       
        [Required]
        public string Title { get; set; }

        public string Text { get; set; } 

        [Required]
        [Range(1, 5, ErrorMessage = "Please enter valid importance between 1 and 5")]
        public int Importance { get; set; }

        public DateTime DoUntil { get; set; }
    }
}
