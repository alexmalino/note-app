﻿namespace note_api.Dtos
{
    public class SettingDto
    {    
        public int SortTypeId { get; set; }

        public int ThemeId { get; set; }

        public bool ShowFinished { get; set; }
    }
}
