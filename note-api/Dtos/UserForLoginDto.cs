﻿namespace note_api.Dtos
{
    public class UserForLoginDto
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}
