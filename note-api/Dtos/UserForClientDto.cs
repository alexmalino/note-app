﻿using System;

namespace note_api.Dtos
{
    public class UserForClientDto
    {
        public int Id { get; set; }

        public string Nickname { get; set; }

        public string EmailAddress { get; set; }   

        public SettingDto Setting { get; set; }

        public DateTime Created { get; set; }
    }
}
