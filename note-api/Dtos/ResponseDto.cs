﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace note_api.Dtos
{
    public class ResponseDto
    {
        public string Message { get; set; }
    }
}
